from django.db import models

class VKUser(models.Model):

    vk_id = models.IntegerField()
    access_token = models.CharField(max_length = 32, blank = True)
    steam_id = models.CharField(max_length = 255, blank = True)

    def __unicode__(self):
        return str(self.vk_id)


class Screenshot(models.Model):

    user = models.ForeignKey(VKUser)
    url = models.CharField(max_length = 255)
    uploaded = models.BooleanField(default = False)

    def __unicode__(self):
        return str(self.url)