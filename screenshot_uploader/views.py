﻿from django.http import HttpResponse
from django.views.generic.base import TemplateView, RedirectView
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from screenshot_uploader.oauth import get_token, call_api
from screenshot_uploader.models import VKUser

class MainPage(TemplateView):

    """
        Class-view for main page
    """
    
    template_name = "index.html" #Name of the template to be rendered

    def get_context_data(self, **kwargs):
        context = super(MainPage, self).get_context_data(**kwargs)
        context['logged'] = self.request.session.get("logged")
        context['avatar'] = self.request.session.get("avatar")
        context['page'] = "main"
        return context

    def get(self, request, *args, **kwargs):
        if request.GET.get("logout"):
            request.session.flush()
            return redirect("main")
        else:
            return super(MainPage, self).get(request, *args, **kwargs)

class AuthPage(RedirectView):

    """
        Class-view for redirect page
    """

    query_string = False

    def get(self, request, *args, **kwargs):
        """
            GET method handler
        """
        response = get_token(request.GET.get('code'), request.build_absolute_uri(reverse("main")) + "auth/") #Trying to get token from GET query string
        if response:
            try:
                usr = VKUser.objects.get(vk_id = int(response['user_id'])) #Search for user in DB
            except:
                usr = VKUser() #If not found, create new
            #Set user info
            usr.access_token = response['access_token']
            usr.vk_id = int(response['user_id'])
            usr.save()
            #Set user session to logged in
            request.session['user_id'] = int(response['user_id'])
            request.session['avatar'] = call_api("users.get", response['access_token'], "fields=photo_50")['response'][0]['photo_50']
            request.session['logged'] = True
        else:
            pass
        return super(AuthPage, self).get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        self.url = reverse("main")
        return super(AuthPage, self).get_redirect_url(*args, **kwargs)

class SettingsPage(TemplateView):

    """
        Class-view for settings page
    """
    template_name = "settings.html" #Name of the template to be rendered


    def get_context_data(self, **kwargs):
        context = super(SettingsPage, self).get_context_data(**kwargs)
        context['logged'] = self.request.session.get("logged")
        context['avatar'] = self.request.session.get("avatar")
        context['page'] = "settings"
        context['steamID'] = VKUser.objects.get(vk_id = int(self.request.session['user_id'])).steam_id
        return context

    def get(self, request, *args, **kwargs):
        if request.session.get("logged"):
            return super(SettingsPage, self).get(request, *args, **kwargs)
        else:
            return redirect("main")

    def post(self, request, *args, **kwargs):
        if request.POST.get("steamIDBox"):
            try:
                usr = VKUser.objects.get(vk_id = request.session['user_id'])
                usr.steam_id = request.POST.get("steamIDBox")
                usr.save()
            except:
                pass
            context = self.get_context_data(**kwargs)
            context['message_type'] = "success"
            context['message'] = "SteamID был успешно изменен!"
            return self.render_to_response(context)
        else:
            pass
