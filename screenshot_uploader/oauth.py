from urllib2 import urlopen, HTTPError
from django.core.urlresolvers import reverse
from json import loads

GET_ACCESS_TOKEN_URL_FORMAT = 'https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&code={2}&redirect_uri={3}'
API_CALL_FORMAT = 'https://api.vk.com/method/{0}?{1}&access_token={2}';
CLIENT_ID = "4351753"
SECRET_KEY = "VIXy4KyapOTvJWkT8p2C"

def get_token(secret, redirect_url):
    try:
        url = urlopen(GET_ACCESS_TOKEN_URL_FORMAT.format(CLIENT_ID, SECRET_KEY, secret, redirect_url))
        json_object = loads(url.read())
        return json_object
    except HTTPError:
        return False

def call_api(method_name, access_token, params):
    try:
        url = urlopen(API_CALL_FORMAT.format(method_name, params, access_token))
        json_object = loads(url.read())
        return json_object
    except:
        return False