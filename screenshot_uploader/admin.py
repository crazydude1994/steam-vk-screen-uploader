from django.contrib import admin
from screenshot_uploader.models import Screenshot, VKUser

admin.site.register(Screenshot)
admin.site.register(VKUser)