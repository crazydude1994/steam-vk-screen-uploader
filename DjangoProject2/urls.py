from django.conf.urls import patterns, include, url
from screenshot_uploader.views import MainPage, AuthPage, SettingsPage

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', MainPage.as_view(), name = "main"),
    url(r'^auth/$', AuthPage.as_view(), name = "auth_page"),
    url(r'^settings/$', SettingsPage.as_view(), name="settings"),
)